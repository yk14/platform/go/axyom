package db

import (
	"gitlab.com/yk14/platform/go/axyom/db/encoding"
)

type database struct {
	codec encoding.Codec
}
