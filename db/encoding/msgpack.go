package encoding

import "github.com/vmihailenco/msgpack/v4"

var MsgPackCodec = Codec{
	Marshal:   msgpack.Marshal,
	Unmarshal: msgpack.Unmarshal,
}

func NewMsgPackCodec() *Codec {
	return &Codec{
		Marshal:   msgpack.Marshal,
		Unmarshal: msgpack.Unmarshal,
	}
}
