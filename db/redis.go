package db

import (
	"gitlab.com/yk14/platform/go/axyom/db/encoding"
	"gitlab.com/yk14/platform/go/axyom/db/redis"
)


type RedisDatabase struct {
	database
	Client redis.Client
}

func NewRedis(client redis.Client) *RedisDatabase {
	d := &RedisDatabase{Client: client}
	d.codec = encoding.DefaultCodec
	return d
}

func (r *RedisDatabase) Codec(codec encoding.Codec) *RedisDatabase {
	r.codec = codec
	return r
}
