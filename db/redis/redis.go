package redis

import (
	"context"
	"errors"
	"fmt"
	"github.com/go-redis/redis/v8"
	"net"
	"strings"
	"time"
)

type Client redis.UniversalClient

type Options struct {
	Addrs          []string
	ClusterOptions *redis.ClusterOptions
	RedisOptions   *redis.Options
}


func NewClientWithTimeout(opt *Options, svrMode string, timeoutSeconds int32) Client {
	// Because unbuffered chan must exist at the same time read-chan and write-chan, if read-chan exits first, it will block the writing of chan, so add a buffer to prevent the blocking of writing chan
	c := make(chan Client, 1)
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	go func() {
		c <- newClientWithContext(opt, svrMode, ctx)
	}()

	select {
	case client := <-c:
		return client
	case <-time.After(time.Duration(timeoutSeconds) * time.Second):
		return nil
	}
}

func newClientWithContext(opt *Options, svrMode string, ctx context.Context) Client {
	if svrMode == "ClusterMode" {
		client, err := TryClusterWithContext(opt, ctx)
		if err != nil {
			fmt.Println("tryClusterWithContext failed, err:", err)
			return nil
		}
		return client
	} else if svrMode == "SentinelMode"{
		return TrySentinelwithContext(opt)
	} else {
		//svrMode == "MasterSlaveMode" or ""
		return TryMasterSlaveWithContext(opt, ctx)
	}
}

func TryMasterSlaveWithContext(opt *Options, ctx context.Context) Client {
	client := redis.NewClient(&redis.Options{Addr: opt.Addrs[0], MaxRetries: 3})
	for {
		select {
		case <-ctx.Done():
			return nil
		default:
			_, err := client.Ping(context.Background()).Result()
			if err == nil {
				return client
			}
		}
	}
}

func TryClusterWithContext(opt *Options, ctx context.Context) (*redis.ClusterClient, error) {
	client := redis.NewClusterClient(&redis.ClusterOptions{Addrs: opt.Addrs, MaxRedirects: 3, MaxRetries: 3})
	for {
		select {
		case <-ctx.Done():
			return nil, errors.New("cancelled")
		default:
			_, err := client.GetSet(context.Background(), "_init", "").Result()
			if err != nil {
				if strings.Contains(err.Error(), "cluster support disabled") || strings.Contains(err.Error(), "unknown command `cluster`") || strings.Contains(err.Error(), "unknown command 'cluster'") {
					return nil, err
				}
			} else {
				return client, nil
			}
		}
	}
}

func TrySentinelwithContext(opt *Options) *redis.Client {
	//var sentinelAddrs []string
	const masterGrpName = "mymaster"
	var sentinel *redis.SentinelClient
	workingSentinelFound := false

	for _, addr := range opt.Addrs {
		sentinel = redis.NewSentinelClient(&redis.Options{
			Addr:       addr,
			MaxRetries: -1,
		})
		//check if sentinel is good. Break the loop once we have a working sentinel
		masterAddr, err := sentinel.GetMasterAddrByName(context.Background(), masterGrpName).Result()
		if err == nil {
			fmt.Println("masterAddr is ", masterAddr)
			workingSentinelFound = true
			//sentinelAddrs = append(sentinelAddrs, addr)
			break
		}
	}
	if !workingSentinelFound {
		fmt.Println("no found workingSentinel")
		return nil
	}
	/*
	otherSentinelAddrs, err := getOtherSentinelsAddrs(context.Background(), sentinel, masterGrpName)
	if err == nil {
		for _, s := range otherSentinelAddrs {
			sentinelAddrs = AppendIfMissing(sentinelAddrs, s)
		}
	}*/
	client := redis.NewFailoverClient(&redis.FailoverOptions{
		MasterName:    masterGrpName,
		SentinelAddrs: opt.Addrs,
		MaxRetries:    -1,
	})
	/*Commenting the code to return the client even if redis master is not ready yet, will revisit later
	_, err = client.Ping(context.Background()).Result()
	if err != nil {
		return nil
	}*/
	return client
}


func getOtherSentinelsAddrs(ctx context.Context, c *redis.SentinelClient, name string) ([]string, error) {
	addrs, err := c.Sentinels(ctx, name).Result()
	if err != nil {
		return []string{}, err
	}
	return parseOtherSentinelAddrs(addrs, false), nil
}

func parseOtherSentinelAddrs(addrs []interface{}, keepDisconnected bool) []string {
	nodes := make([]string, 0, len(addrs))
	for _, node := range addrs {
		ip := ""
		port := ""
		flags := []string{}
		lastkey := ""
		isDown := false

		for _, key := range node.([]interface{}) {
			switch lastkey {
			case "ip":
				ip = key.(string)
			case "port":
				port = key.(string)
			case "flags":
				flags = strings.Split(key.(string), ",")
			}
			lastkey = key.(string)
		}

		for _, flag := range flags {
			switch flag {
			case "s_down", "o_down":
				isDown = true
			case "disconnected":
				if !keepDisconnected {
					isDown = true
				}
			}
		}

		if !isDown {
			nodes = append(nodes, net.JoinHostPort(ip, port))
		}
	}

	return nodes
}

func AppendIfMissing(slice []string, s string) []string {
	for _, ele := range slice {
		if ele == s {
			return slice
		}
	}
	return append(slice, s)
}
