module gitlab.com/yk14/platform/go/axyom

go 1.17

require (
	github.com/go-redis/redis/v8 v8.11.4
	github.com/vmihailenco/msgpack/v4 v4.2.1
)

require (
	github.com/cespare/xxhash/v2 v2.1.2 // indirect
	github.com/dgryski/go-rendezvous v0.0.0-20200823014737-9f7001d12a5f // indirect
	github.com/golang/protobuf v1.5.2 // indirect
	github.com/vmihailenco/tagparser v0.1.0 // indirect
	golang.org/x/net v0.0.0-20210428140749-89ef3d95e781 // indirect
	google.golang.org/appengine v1.6.1 // indirect
	google.golang.org/protobuf v1.26.0 // indirect
)
