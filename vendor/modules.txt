# github.com/cespare/xxhash/v2 v2.1.2
## explicit; go 1.11
github.com/cespare/xxhash/v2
# github.com/dgryski/go-rendezvous v0.0.0-20200823014737-9f7001d12a5f
## explicit
github.com/dgryski/go-rendezvous
# github.com/go-redis/redis/v8 v8.11.4
## explicit; go 1.13
github.com/go-redis/redis/v8
github.com/go-redis/redis/v8/internal
github.com/go-redis/redis/v8/internal/hashtag
github.com/go-redis/redis/v8/internal/hscan
github.com/go-redis/redis/v8/internal/pool
github.com/go-redis/redis/v8/internal/proto
github.com/go-redis/redis/v8/internal/rand
github.com/go-redis/redis/v8/internal/util
# github.com/golang/protobuf v1.5.2
## explicit; go 1.9
github.com/golang/protobuf/proto
# github.com/vmihailenco/msgpack/v4 v4.2.1
## explicit; go 1.13
github.com/vmihailenco/msgpack/v4
github.com/vmihailenco/msgpack/v4/codes
# github.com/vmihailenco/tagparser v0.1.0
## explicit; go 1.13
github.com/vmihailenco/tagparser
github.com/vmihailenco/tagparser/internal
github.com/vmihailenco/tagparser/internal/parser
# golang.org/x/net v0.0.0-20210428140749-89ef3d95e781
## explicit; go 1.11
golang.org/x/net/context
# google.golang.org/appengine v1.6.1
## explicit
google.golang.org/appengine
google.golang.org/appengine/datastore
google.golang.org/appengine/datastore/internal/cloudkey
google.golang.org/appengine/datastore/internal/cloudpb
google.golang.org/appengine/internal
google.golang.org/appengine/internal/app_identity
google.golang.org/appengine/internal/base
google.golang.org/appengine/internal/datastore
google.golang.org/appengine/internal/log
google.golang.org/appengine/internal/modules
google.golang.org/appengine/internal/remote_api
# google.golang.org/protobuf v1.26.0
## explicit; go 1.9
google.golang.org/protobuf/encoding/prototext
google.golang.org/protobuf/encoding/protowire
google.golang.org/protobuf/internal/descfmt
google.golang.org/protobuf/internal/descopts
google.golang.org/protobuf/internal/detrand
google.golang.org/protobuf/internal/encoding/defval
google.golang.org/protobuf/internal/encoding/messageset
google.golang.org/protobuf/internal/encoding/tag
google.golang.org/protobuf/internal/encoding/text
google.golang.org/protobuf/internal/errors
google.golang.org/protobuf/internal/filedesc
google.golang.org/protobuf/internal/filetype
google.golang.org/protobuf/internal/flags
google.golang.org/protobuf/internal/genid
google.golang.org/protobuf/internal/impl
google.golang.org/protobuf/internal/order
google.golang.org/protobuf/internal/pragma
google.golang.org/protobuf/internal/set
google.golang.org/protobuf/internal/strs
google.golang.org/protobuf/internal/version
google.golang.org/protobuf/proto
google.golang.org/protobuf/reflect/protodesc
google.golang.org/protobuf/reflect/protoreflect
google.golang.org/protobuf/reflect/protoregistry
google.golang.org/protobuf/runtime/protoiface
google.golang.org/protobuf/runtime/protoimpl
google.golang.org/protobuf/types/descriptorpb
